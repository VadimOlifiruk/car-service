import { LightningElement, track } from 'lwc';

export default class CarService extends LightningElement {

    @track showstepOne = true;
    @track showstepTwo = false;
    @track showstepThree = false;
    @track showstepFour = false;

    @track workTypeId;
    @track workOrderId;
    @track productId;

    handleStepOne(event){

        if (event.detail.result) {

            this.workTypeId = event.detail.id;
         
            this.showstepOne = false;
            this.showstepTwo = true;
        }
    }

    handleStepTwo(event) {
        
        if (event.detail.result) {
               
            this.showstepTwo = false;
            this.showstepThree = true;
        }
    }

    handleStepThree(event) {    

        if (event.detail.result) {      
            
            this.productId = event.detail.id;
         
            this.showstepThree = false;
            this.showstepFour = true;
        }
        
    }

    /*
    handleStepFour(event) {
        
        if (event.detail.result) {            

            this.showstepFour = false;
            this.showstepFive = true;
        }
    }*/ 
}