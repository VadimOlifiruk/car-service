import { LightningElement, api } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class StepThree extends LightningElement {
   
    @api workType;
    product;
    quantity;
    measure;

    workTypeChangedHandler(event) {
        this.workType = event.target.value;      
    }

    productChangedHandler(event) {
        this.product = event.target.value;  
    }

    quantityChangedHandler(event) {
        this.quantity = event.target.value;  
    }

    measureChangedHandler(event) {        
        this.measure = event.target.value;        
    }

    createProductRequired () {
       
        let isValid = true;

        this.template.querySelectorAll('lightning-input-field').forEach(element => { 
                      
            isValid = element.reportValidity() && isValid;
        });
                     
        if (isValid) {  

            let fields = {'ParentRecordId': this.workType, 'Product2Id': this.product, 'QuantityRequired' : this.quantity, 'QuantityUnitOfMeasure': this.measure};
            let objRecordInput = {'apiName' : 'ProductRequired', fields};
         
            createRecord(objRecordInput).then(response => {          
           
                const e = new CustomEvent('stepthree', {
                  
                    detail: {result:"true", id: this.product}
                    });
                    
                    this.dispatchEvent(e);    
    
    
            }).catch(error => {

                const evt = new ShowToastEvent({                                   
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);

            });            
        }
    }

}