import { LightningElement, api, track} from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class StepTwo extends LightningElement {

    @api workType;
    value = {}
    selectedRecordId;

    skill;
    skillLevel;

    selectedRecordId;

    workTypeChangedHandler(event){
        this.workType = event.target.value;  
    }

    skillChangedHandler(event){
        this.skill = event.target.value;  
    }

    skillLevelChangedHandler(event){
        this.skillLevel = event.target.value;  
    }

    createSkillRequirement () {

        let isValid = true;

        this.template.querySelectorAll('lightning-input-field').forEach(element => { 
            isValid = element.reportValidity() && isValid;
        });
            
        
        if (isValid) {  

            let fields = {'RelatedRecordId': this.workType, 'SkillId': this.skill, 'SkillLevel' : this.skillLevel};
            let objRecordInput = {'apiName' : 'SkillRequirement', fields};

            createRecord(objRecordInput).then(response => {          

                const e = new CustomEvent('steptwo', {
                  
                    detail: {result:"true", id: response.id}
                    });
                    
                    this.dispatchEvent(e);    
    
    
            }).catch(error => {

                const evt = new ShowToastEvent({                                         
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);
            });            
        }
    }
}