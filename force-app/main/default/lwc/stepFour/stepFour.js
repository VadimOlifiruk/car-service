import { LightningElement, api } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class StepFour extends LightningElement {

    @api product;
    location;    
    quantity;
    measure;
    serialNumber;

    productChangedHandler(event){
        this.product = event.target.value;  
    }

    locationChangedHandler(event){
        this.location = event.target.value;  
    }

    quantityChangedHandler(event){
        this.quantity = event.target.value;  
    }

    measureChangedHandler(event){
        this.measure = event.target.value;  
    }

    serialNumberChangedHandler(event){
        this.serialNumber = event.target.value;  
    }

    createProductItem() {

        let isValid = true;

        this.template.querySelectorAll('lightning-input-field').forEach(element => { 
                      
            isValid = element.reportValidity() && isValid;
        });


        if (isValid) {  

            let fields = {'Product2Id': this.product, 'LocationId': this.location, 'QuantityOnHand' : this.quantity, 'QuantityUnitOfMeasure' : this.measure, 'SerialNumber' : this.serialNumber};
            let objRecordInput = {'apiName' : 'ProductItem', fields};
           
            createRecord(objRecordInput).then(response => {          

                const evtSuccess = new ShowToastEvent({                                          
                    message: `Record ${response.id} added`                   
                });

                this.dispatchEvent(evtSuccess); 
    
    
            }).catch(error => {
                
                const evt = new ShowToastEvent({                                       
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);

            });            
        }
    }

}