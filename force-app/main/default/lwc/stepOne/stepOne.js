import { LightningElement} from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class StepOne extends LightningElement {

    workType;  

    recordId;

    workTypeName;
    description;
    estimatedDuration;
    durationType;
    isServiceAppointment = true;    

    get options() {
        return [           
            { label: 'Minutes', value: 'Minutes' },
            { label: 'Hours', value: 'Hours' },
        ];
    }

    workTypeNameChangedHandler(event){
        this.workTypeName = event.target.value;  
    }

    descriptionChangedHandler(event){
        this.description = event.target.value;  
    }

    estimatedDurationChangedHandler(event){
        this.estimatedDuration = event.target.value;  
    }

    durationTypeChangedHandler(event){
        this.durationType = event.target.value;       
    }  
    
    appointementChangedHandler(event) {    
        this.isServiceAppointment = event.target.checked;  
    }
    
    createWorkType() {
        
        let isvalid = true;

        this.template.querySelectorAll('lightning-input').forEach(element => {            
            element.reportValidity();
            isvalid = isvalid && element.checkValidity();
        });

        if (isvalid) {  

            let fields = {'Name': this.workTypeName, 'Description': this.description, 'DurationType' : this.durationType, 'EstimatedDuration' : this.estimatedDuration, 'ShouldAutoCreateSvcAppt': this.isServiceAppointment};
            let objRecordInput = {'apiName' : 'WorkType', fields};           

            createRecord(objRecordInput).then(response => {          

                const e = new CustomEvent('stepone', {
                  
                    detail: {result:"true", id: response.id}
                    });
                  
                    this.dispatchEvent(e);    
    
    
            }).catch(error => {

                const evt = new ShowToastEvent({                                     
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);

            });            
        }   
    }

}